namespace ConnectorLib
{
	public sealed class FreezeInfo
	{
		public readonly long address;
		
		public readonly string domain;
		
		public long value;
		
		public readonly int size;
		
		public readonly uint mask;
		
		public readonly WriteType writeType;
		
		public readonly long cmpAddress;
		
		public readonly long cmpValue;
		
		public readonly int cmpSize;
		
		public readonly CompareType condition;

		public FreezeInfo(
			long address,
			string domain,
			long value,
			int size,
			uint mask,
			WriteType writeType,
			long cmpAddress,
			long cmpValue,
			int cmpSize,
			CompareType condition)
		{
			this.address = address;
			this.domain = domain;
			this.value = value;
			this.size = size;
			this.mask = mask;
			this.writeType = writeType;
			this.cmpAddress = cmpAddress;
			this.cmpValue = cmpValue;
			this.cmpSize = cmpSize;
			this.condition = condition;
		}
	}
}
