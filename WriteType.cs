using System.ComponentModel;

namespace ConnectorLib
{
	public enum WriteType : byte
	{
		None = 0,
		WriteValue = 1,
		IncrementBy = 2,
		DecrementBy = 3,
		WriteMasked = 4,
	}
}
