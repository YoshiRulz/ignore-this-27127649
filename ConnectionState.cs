using System;
using System.Drawing;
using System.Net.WebSockets;

namespace ConnectorLib
{
	public abstract class ConnectionState
	{
		public static readonly ConnectionState CONNECTING = new ConnectingState();

		public static readonly ConnectionState CONNECTED = new ConnectedState();

		public static readonly ConnectionState EXIT = new ExitState();

		public virtual void Enter(ConnectorMemory memory) {}

		public virtual void Exit(ConnectorMemory memory) {}

		public virtual void Tick(ConnectorMemory memory) {}

		private sealed class ConnectingState : ConnectionState
		{
			public override void Enter(ConnectorMemory memory)
			{
				memory.draw_begin();
				var y = memory.draw_get_framebuffer_height() / 2;
				memory.draw_text(2, y, "Connecting to ConnectorLib host...", Color.Red, Color.Black);
				memory.draw_end();

				memory.pause();
			}

			public override void Exit(ConnectorMemory memory)
			{
				memory.draw_clear();
				memory.unpause();
			}

			public override void Tick(ConnectorMemory memory)
			{
				var currTime = DateTime.Now;
				if (memory.lastTime.AddSeconds(memory.RECONNECT_INTERVAL) <= currTime)
				{
					memory.lastTime = currTime;
					memory.tcp = memory.APIs.Comm.WebSockets?.Open(new Uri($"wss://{memory.HOST_ADDRESS}:{memory.HOST_PORT}"));

					if (memory.tcp == null || memory.tcp.Value.State == WebSocketState.Closed)
					{
						Console.WriteLine("Failed to open socket");
						memory.disconnect();
					}
					else
					{
						memory.message("Connection established");
						memory.SetConnectionState(CONNECTED);
					}
				}
			}
		}

		private sealed class ConnectedState : ConnectionState
		{
			public override void Exit(ConnectorMemory memory)
			{
				memory.disconnect();
			}

			public override void Tick(ConnectorMemory memory)
			{
				memory.receive();
				memory.applyFreezes();
			}
		}

		private sealed class ExitState : ConnectionState
		{
			public override void Enter(ConnectorMemory memory)
			{
				memory.disconnect();
			}
		}
	}
}
