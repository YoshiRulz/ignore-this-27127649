using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.WebSockets;
using System.Text.Json;

using BizHawk.Client.Common;

namespace ConnectorLib
{
	public sealed class ConnectorMemory
	{
		public static readonly int VERSION_MAJOR = 2;

		public static readonly int VERSION_MINOR = 2;

		public static readonly int VERSION_PATCH = 0;

		public static readonly string VERSION = $"{VERSION_MAJOR}.{VERSION_MINOR}.{VERSION_PATCH}";

		public readonly string HOST_ADDRESS = "127.0.0.1";

		public readonly int HOST_PORT = 43884;

		public readonly int RECONNECT_INTERVAL = 3;

		public readonly int KEEPALIVE_DELAY = 5;

		public readonly ApiContainer APIs;

		private DateTime currTime;

		public DateTime lastTime;

		private int receiveSize;

		private ConnectionState _currentState = ConnectionState.CONNECTING;

		public ClientWebSocketWrapper? tcp;

		private int receivePart;

		public readonly IDictionary<long, FreezeInfo> memFreezes = new Dictionary<long, FreezeInfo>();

		public ConnectorMemory(ApiContainer apis) => APIs = apis;

		public void SetConnectionState(ConnectionState newState)
		{
			_currentState.Exit(this);
			_currentState = newState;
			_currentState.Enter(this);
		}

		public void TickConnectionState() => _currentState.Tick(this);

		public void applyFreezes()
		{
			foreach (var kvp in memFreezes)
			{
				var k = kvp.Key;
				var fz = kvp.Value;
				var sz = fz.size;
				if (checkCond(fz))
				{
					var wType = fz.writeType;
					Func<long, string, uint> rF;
					Action<long, uint, string> wF;
					if (sz == 1 && wType > 0)
					{
						rF = read_u8;
						wF = write_u8;
					}
					else if (sz == 2)
					{
						rF = read_u16_le;
						wF = write_u16_le;
					}
					else if (sz == 4)
					{
						rF = read_u32_le;
						wF = write_u32_le;
					}
					else
					{
						throw new Exception();
					}

					var val = fz.writeType switch
					{
						WriteType.WriteValue => (uint) fz.value,
						WriteType.IncrementBy => rF(fz.address, fz.domain) + (uint) fz.value,
						WriteType.DecrementBy => rF(fz.address, fz.domain) - (uint) fz.value,
						WriteType.WriteMasked => ((uint) fz.value & fz.mask) | (rF(fz.address, fz.domain) & ~fz.mask),
						_ => throw new Exception()
					};

					wF(fz.address, val, fz.domain);
				}
			}
		}

		public bool checkCond(FreezeInfo fz)
		{
			var cond = fz.condition;
			if (cond == CompareType.Always) return true;

			uint result;
			var size = fz.cmpSize;
			if (((byte) cond & 0x80) == 0x80)
			{
				result = (uint) framecount();
				cond = (CompareType) ((byte) cond & 0x0F);
			}
			else
			{
				result = size switch
				{
					1 => read_u8(fz.cmpAddress, fz.domain),
					2 => read_u16_le(fz.cmpAddress, fz.domain),
					4 => read_u32_le(fz.cmpAddress, fz.domain),
					_ => throw new Exception()
				};
			}

			return cond switch
			{
				CompareType.Equal => result == fz.cmpValue,
				CompareType.NotEqual => result != fz.cmpValue,
				CompareType.GreaterThan => result > fz.cmpValue,
				CompareType.GreaterThanOrEqual => result >= fz.cmpValue,
				CompareType.LessThan => result < fz.cmpValue,
				CompareType.LessThanOrEqual => result <= fz.cmpValue,
				CompareType.MaskSet => (fz.cmpValue & result) == fz.cmpValue,
				CompareType.MaskUnset => (fz.cmpValue & result) != fz.cmpValue,
				_ => false
			};
		}

		public void disconnect()
		{
			if (tcp != null)
			{
				tcp.Value.Close(WebSocketCloseStatus.Empty, string.Empty);
				tcp = null;
			}
		}

		public uint read_u8(long address, string? domain = null) => APIs.Memory.ReadU8(address, domain);

		public uint read_u16_le(long address, string domain)
		{
			APIs.Memory.SetBigEndian(false);
			return APIs.Memory.ReadU16(address, domain);
		}

		public uint read_u32_le(long address, string domain)
		{
			APIs.Memory.SetBigEndian(false);
			return APIs.Memory.ReadU32(address, domain);
		}

		public void write_u8(long address, uint value, string domain) => APIs.Memory.WriteU8(address, value, domain);

		public void write_u16_le(long address, uint value, string domain)
		{
			APIs.Memory.SetBigEndian(false);
			APIs.Memory.WriteU16(address, value, domain);
		}

		public void write_u32_le(long address, uint value, string domain)
		{
			APIs.Memory.SetBigEndian(false);
			APIs.Memory.WriteU32(address, value, domain);
		}

		public List<byte> read_byte_range(long address, int length, string domain) => APIs.Memory.ReadByteRange(address, length, domain);

		public void write_byte_range(long address, List<byte> byteRange, string domain) => APIs.Memory.WriteByteRange(address, byteRange, domain);

		public string pack_byte_range(List<byte> halByteBuffer) => Convert.ToBase64String(halByteBuffer.ToArray());

		public List<byte> unpack_byte_range(string packedBuffer) => new List<byte>(Convert.FromBase64String(packedBuffer));

		public void open_rom(string path) => APIs.EmuClient.OpenRom(path);

		public void close_rom() => APIs.EmuClient.CloseRom();

		public string get_rom_path() => APIs.GameInfo.GetRomName();

		public string get_system_id() => APIs.Emulation.GetSystemId();

		public void message(string s)
		{
			APIs.Gui.AddMessage(s);
			Console.WriteLine(s);
		}

		public void pause() => APIs.EmuClient.Pause();

		public void unpause() => APIs.EmuClient.Unpause();

		public int draw_get_framebuffer_height() => APIs.EmuClient.BufferHeight();

		public void draw_begin() => APIs.Gui.DrawNew("emu", clear: true);

		public void draw_end() => APIs.Gui.DrawFinish();

		public void draw_text(int x, int y, string msg, Color textColor, Color backColor) => APIs.Gui.DrawText(x, y, msg, textColor, backColor);

		public void draw_clear()
		{
			draw_begin();
			draw_end();
		}

		public int framecount() => APIs.Emulation.FrameCount();

		public string? receiveData(int n)
		{
			var data = tcp?.Receive(n - receivePart) ?? null;
			var nRec = data?.Length ?? 0;
			if (nRec != n)
			{
				if (tcp?.State != WebSocketState.Open)
				{
					Console.WriteLine("Connection lost");
					reconnect();
				}
				else
				{
					receivePart = nRec;
				}
			}
			else
			{
				receivePart = 0;
			}
			return data;
		}

		public void removeHold(long addr) => memFreezes.Remove(addr);

		public void processBlock(JsonElement root)
		{
			var commandType = (CommandType) root.GetProperty("type").GetByte();
			var domain = root.GetProperty("domain").GetString();
			var address = root.GetProperty("address").GetInt64();
			var value = root.GetProperty("value").GetInt64();
			var size = root.GetProperty("size").GetInt32();
			var result = new ProcessedResult(
				root.GetProperty("id").GetGuid(),
				DateTime.Now,
				commandType,
				address,
				size,
				domain,
				value
			);
			switch (commandType)
			{
				case CommandType.ReadU8:
					result.value = read_u8(address, domain);
					break;
				case CommandType.ReadU16:
					result.value = read_u16_le(address, domain);
					break;
				case CommandType.ReadU32:
					result.value = read_u32_le(address, domain);
					break;
				case CommandType.ReadU64:
				case CommandType.ReadS8:
				case CommandType.ReadS16:
				case CommandType.ReadS32:
				case CommandType.ReadS64:
				case CommandType.ReadBoolean:
				case CommandType.ReadString:
				case CommandType.ReadState:
					break;
				case CommandType.ReadBlock:
					result.block = pack_byte_range(read_byte_range(address, (int) value, domain));
					break;
				case CommandType.WriteU8:
					write_u8(address, (uint) value, domain);
					if (memFreezes.TryGetValue(address, out var freeze8)) freeze8.value = value;
					break;
				case CommandType.WriteU16:
					write_u16_le(address, (uint) value, domain);
					if (memFreezes.TryGetValue(address, out var freeze16)) freeze16.value = value;
					break;
				case CommandType.WriteU32:
					write_u32_le(address, (uint) value, domain);
					if (memFreezes.TryGetValue(address, out var freeze32)) freeze32.value = value;
					break;
				case CommandType.WriteU64:
				case CommandType.WriteS8:
				case CommandType.WriteS16:
				case CommandType.WriteS32:
				case CommandType.WriteS64:
				case CommandType.WriteBoolean:
				case CommandType.WriteString:
				case CommandType.WriteState:
					break;
				case CommandType.WriteBlock:
					var m = unpack_byte_range(root.GetProperty("block").GetString());
					write_byte_range(address, m, domain);
					break;
				case CommandType.SetBits:
					var oldSet = read_u8(address, domain);
					write_u8(address, oldSet | (uint) value, domain);
					// block['value'] = oldSet
					break;
				case CommandType.UnsetBits:
					var oldUnset = read_u8(address);
					write_u8(address, oldUnset & ~(uint) value, domain);
					// block['value'] = oldUnset
					break;
				case CommandType.CompareSwap:
					break;
				case CommandType.FreezeU:
					memFreezes[address] = new FreezeInfo(
						address,
						domain,
						value,
						size,
						0xFF,
						(WriteType) root.GetProperty("writeType").GetByte(),
						root.GetProperty("cmpAddress").GetInt64(),
						root.GetProperty("cmpValue").GetInt64(),
						root.GetProperty("cmpSize").GetInt32(),
						(CompareType) root.GetProperty("condition").GetByte()
					);
					break;
				case CommandType.Unfreeze:
					removeHold(address);
					break;
				case CommandType.OpenROM:
					open_rom(root.GetProperty("message").GetString());
					break;
				case CommandType.CloseROM:
					close_rom();
					break;
				case CommandType.GetROMPath:
					result.message = get_rom_path();
					break;
				case CommandType.GetEmulationCore:
					var a = (value >> 16) & 0xFF;
					var b = (value >> 8) & 0xFF;
					var c = value & 0xFF;
//					Console.WriteLine($"Server version {a}.{b}.{c}");

					var major = VERSION_MAJOR << 16;
					var minor = VERSION_MINOR << 8;
					result.value = major | minor | VERSION_PATCH;
					result.message = get_system_id();
					break;
				case CommandType.SendMessage:
					message(root.GetProperty("message").GetString());
					break;
				case CommandType.KeepAlive:
					break;
				default:
					Console.WriteLine($"Unknown block type received: {commandType}");
					return;
			}
			sendBlock(result);
		}

		public void sendBlock(ProcessedResult block)
		{
			var data = JsonSerializer.Serialize(block);
			tcp.Value.Send(data, true);
		}

		public void receive()
		{
			currTime = DateTime.Now;

			while (true)
			{
				if (receiveSize == 0)
				{
					var n = receiveData(4);
					if (n == null) break;

					var n1 = n[0]; var n2 = n[1]; var n3 = n[2]; var d = n[3];
					var a = n1 << 24;
					var b = n2 << 16;
					var c = n3 << 8;
					receiveSize = a | b | c | d;
				}

				if (receiveSize != 0)
				{
					var data = receiveData(receiveSize);
					if (data == null) break;

//					Console.WriteLine($"recv {data}");
					using var parsed = JsonDocument.Parse(data);
					processBlock(parsed.RootElement);
					receiveSize = 0;
				}

				lastTime = currTime;
			}

			if (lastTime.AddSeconds(KEEPALIVE_DELAY) < currTime)
			{
				Console.WriteLine("Keepalive failed");
				reconnect();
			}
		}

		public void reconnect()
		{
			if (_currentState != ConnectionState.EXIT) SetConnectionState(ConnectionState.CONNECTING);
		}
	}
}
