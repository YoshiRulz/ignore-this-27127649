using System.ComponentModel;

namespace ConnectorLib
{
	public enum CompareType : byte
	{
		Never = 0x00,
		Equal = 0x01,
		NotEqual = 0x02,//6,A,C,E
		Always = 0x03,//7,B,D,F
		GreaterThan = 0x04,
		GreaterThanOrEqual = 0x05,
		LessThan = 0x08,
		LessThanOrEqual = 0x09,
		NotLessThan = 0x05,
		NotGreaterThan = 0x09,

		MaskSet = 0x11,
		MaskUnset = 0x12,

		AndThenEqual = 0x11,
		AndThenNotEqual = 0x12,

		TimeEqual = 0x81,
		TimeNotEqual = 0x82,
		TimeGreaterThan = 0x84,
		TimeGreaterThanOrEqual = 0x85,
		TimeLessThan = 0x88,
		TimeLessThanOrEqual = 0x89,
		TimeNotLessThan = 0x85,
		TimeNotGreaterThan = 0x89
	}
}
