﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using BizHawk.Client.Common;

namespace ConnectorLib
{
	[ExternalTool("ConnectorLib ApiHawk Connector", Description = "links EmuHawk to the Crowd Control app")]
	public class ConnectorToolForm : Form, IExternalToolForm
	{
		[RequiredApi]
		public ICommApi? _maybeCommAPI { get; set; }

		[RequiredApi]
		public IEmuClientApi? _maybeClientAPI { get; set; }

		[RequiredApi]
		public IEmulationApi? _maybeEmuAPI { get; set; }

		[RequiredApi]
		public IGameInfoApi? _maybeGameInfoAPI { get; set; }

		[RequiredApi]
		public IGuiApi? _maybeGuiAPI { get; set; }

		[RequiredApi]
		public IMemoryApi? _maybeMemAPI { get; set; }

		private ApiContainer? _apis;

		private ConnectorMemory? _memory;

		private ApiContainer APIs => _apis ??= new ApiContainer(new Dictionary<Type, IExternalApi>
		{
			[typeof(ICommApi)] = _maybeCommAPI ?? throw new NullReferenceException(),
			[typeof(IEmuClientApi)] = _maybeClientAPI ?? throw new NullReferenceException(),
			[typeof(IEmulationApi)] = _maybeEmuAPI ?? throw new NullReferenceException(),
			[typeof(IGameInfoApi)] = _maybeGameInfoAPI ?? throw new NullReferenceException(),
			[typeof(IGuiApi)] = _maybeGuiAPI ?? throw new NullReferenceException(),
			[typeof(IMemoryApi)] = _maybeMemAPI ?? throw new NullReferenceException()
		});

		private ConnectorMemory Memory => _memory ??= new ConnectorMemory(APIs);

		public ConnectorToolForm()
		{
			SuspendLayout();
			AutoScaleDimensions = new SizeF(6F, 13F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(284, 261);
			Name = "CustomMainForm";
			base.Text = "HelloWorld";
			Controls.Add(new Label { AutoSize = true, Text = "loaded" });
			ResumeLayout();
		}

		public bool AskSaveChanges() => true;

		public void Restart() {} // core restart shouldn't affect sockets

		public void UpdateValues(ToolFormUpdateType type)
		{
			if (type != ToolFormUpdateType.PreFrame && type != ToolFormUpdateType.FastPreFrame) return;
			if (APIs.Emulation.GetSystemId() == "NULL") return;
			Memory.TickConnectionState();
		}

		protected override void Dispose(bool disposing)
		{
			Memory.SetConnectionState(ConnectionState.EXIT);
			base.Dispose(disposing);
		}
	}
}
