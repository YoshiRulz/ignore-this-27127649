using System;

namespace ConnectorLib
{
	public struct ProcessedResult
	{
		public readonly Guid id;

		public readonly DateTime stamp;

		public readonly CommandType type;

		public string message;

		public readonly long address;

		public readonly int size;

		public readonly string domain;

		public long value;

		public string? block;

		public ProcessedResult(
			Guid id,
			DateTime stamp,
			CommandType type,
			long address,
			int size,
			string domain,
			long value)
		{
			this.id = id;
			this.stamp = stamp;
			this.type = type;
			message = string.Empty;
			this.address = address;
			this.size = size;
			this.domain = domain;
			this.value = value;
			block = null;
		}
	}
}
