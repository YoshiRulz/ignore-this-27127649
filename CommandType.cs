using System.ComponentModel;

namespace ConnectorLib
{
	/// <summary>
	/// The block command type enumeration.
	/// </summary>
	public enum CommandType : byte
	{
		[Description("Read U8")]
		ReadU8 = 0x00,
		[Description("Read U16")]
		ReadU16 = 0x01,
		[Description("Read U32")]
		ReadU32 = 0x02,
		[Description("Read U64")]
		ReadU64 = 0x03,
		[Description("Read S8")]
		ReadS8 = 0x04,
		[Description("Read S16")]
		ReadS16 = 0x05,
		[Description("Read S32")]
		ReadS32 = 0x06,
		[Description("Read S64")]
		ReadS64 = 0x07,
		[Description("Read Boolean")]
		ReadBoolean = 0x0C,
		[Description("Read String")]
		ReadString = 0x0D,
		[Description("Read State")]
		ReadState = 0x0E,
		[Description("Read Block")]
		ReadBlock = 0x0F,
		[Description("Write U8")]
		WriteU8 = 0x10,
		[Description("Write U16")]
		WriteU16 = 0x11,
		[Description("Write U32")]
		WriteU32 = 0x12,
		[Description("Write U64")]
		WriteU64 = 0x13,
		[Description("Write S8")]
		WriteS8 = 0x14,
		[Description("Write S16")]
		WriteS16 = 0x15,
		[Description("Write S32")]
		WriteS32 = 0x16,
		[Description("Write S64")]
		WriteS64 = 0x17,
		[Description("Write Boolean")]
		WriteBoolean = 0x1C,
		[Description("Write String")]
		WriteString = 0x1D,
		[Description("Write State")]
		WriteState = 0x1E,
		[Description("Write Block")]
		WriteBlock = 0x1F,
		[Description("Set Bits")]
		SetBits = 0x20,
		[Description("Unset Bits")]
		UnsetBits = 0x21,
		[Description("Compare Swap")]
		CompareSwap = 0x22,
		[Description("Freeze Unsigned")]
		FreezeU = 0x30,
		[Description("Unfreeze")]
		Unfreeze = 0x3F,
		[Description("Load ROM")]
		OpenROM = 0xE0,
		[Description("Unload ROM")]
		CloseROM = 0xE1,
		[Description("Get ROM Path")]
		GetROMPath = 0xE2,
		[Description("Get Emulation Core")]
		GetEmulationCore = 0xE3,
		[Description("Send Message")]
		SendMessage = 0xF0,
		[Description("KeepAlive")]
		KeepAlive = 0xFF
	}
}
